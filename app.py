from flask import Flask, request
from typing import List
import os
import datetime
import requests

app = Flask(__name__)


def construct_log_message(name: str, labels: List[str], checklist: List[str], comment: str):
    """
    Construct the log message
    :param name: Card title
    :param labels: list of label names
    :param checklist: list of checklist items
    :param comment: last comment made on the card
    :return: string
    """
    date = datetime.date.today().isoformat()
    checklist_string = "" if not checklist else "\n".join(
        map(lambda item: f"  - [{'x' if item['state'] == 'complete' else ' '}] {item['name']}", checklist)) + "\n"
    comment_string = f"({comment})" if comment else ''

    return f"- {date}: {''.join([f'[{label}]' for label in labels])} {name} {comment_string}\n" + checklist_string

def get_card(id: str):
    """
    Return JSON response of a card
    :param id: model ID of card
    :return: dictionary
    """
    return requests.get(f"https://api.trello.com/1/cards/{id}", params={
        'key': os.environ['TRELLO_KEY'],
        'token': os.environ['TRELLO_TOKEN'],
    }).json()


def get_last_comment(id):
    """
    Get the last comment on the card, or None
    :param id: model ID of the card
    :return: string
    """
    comments = requests.get(f"https://api.trello.com/1/cards/{id}/actions", params={
        'key': os.environ['TRELLO_KEY'],
        'token': os.environ['TRELLO_TOKEN'],
        # 'filter': 'commentCard',
        # 'fields': 'data',
    }, ).json()

    # filter down to comment updates
    comments_filtered = list(filter(lambda update: update['type'] == 'commentCard', comments))

    if len(comments_filtered) > 0:
        return comments_filtered[0]['data']['text']


def get_checklist(id):
    """
    Get the top checklist and return items
    :param id: card ID
    :return:
    """
    checklists = requests.get(f"https://api.trello.com/1/cards/{id}/checklists", params={
        'key': os.environ['TRELLO_KEY'],
        'token': os.environ['TRELLO_TOKEN'],
        'checkItem_fields': 'name,state'
    }).json()

    if len(checklists) > 0:
        return checklists[0]['checkItems']


def log_message(message, title):
    """
    Append the message to the log
    :param message: string
    :param title: commit title
    """

    # log to file
    with open(os.path.join(os.environ['WIKI_ROOT'], os.environ['TASK_HISTORY_FILE']), 'a') as f:
        f.write(message)

    # add to git
    os.chdir(os.environ['WIKI_ROOT'])
    os.system(f"git add {os.environ['TASK_HISTORY_FILE']}")
    os.system(f"git commit -am \"Tasks: {title}\" ")
    os.system("git pull --quiet --no-edit")
    os.system("git push --quiet")


@app.route('/', methods=['POST', 'GET', 'HEAD'])
def hello_world():
    # Allow Get endpoints so webhooks resolve
    if request.method == 'GET' or request.method == 'HEAD':
        return 'Hello World!'
    elif request.method == 'POST':
        app.logger.info(request.json)

        # simple verification of request body for "security" purposes
        # (Not really that secure, but the ID and board ID are private so they serve as a basic token)
        if "model" in request.json:
            if 'id' not in request.json['model'] or request.json['model']['id'] != os.environ['MODELID']:
                raise ValueError("Invalid Model")
            if 'idBoard' not in request.json['model'] or request.json['model']['idBoard'] != os.environ['BOARDID']:
                raise ValueError("Invalid Board")
        else:
            raise ValueError("Input Invalid")

        # If the webhook event was one we care about, log it
        if request.json['action']['type'] == 'updateCard' \
                and request.json['action']['display']['translationKey'] == 'action_move_card_from_list_to_list':
            card = get_card(request.json['action']['data']['card']['id'])

            name = card['name']
            labels = list(map(lambda label: label['name'], card['labels']))
            checklist = get_checklist(card['id'])
            comment = get_last_comment(card['id'])

            message = construct_log_message(name, labels, checklist, comment)
            log_message(message, name)

        return "Hello World!"


if __name__ == '__main__':
    app.run(debug=True)
